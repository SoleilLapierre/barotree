﻿using CowTools.Extensions;
using CowTools.Xml;
using System.Collections.Generic;
using System.Xml;

namespace BaroTree
{
    /// <summary>
    /// Groups nodes to help reduce graph complexity - maybe.
    /// </summary>
    public class DefaultClassifier : IClassifier
    {
        public string Classify(XmlNode aNode)
        {
            // Hand-rolled heuristics based on data inspection.
            var id = aNode?.GetAttributeOrNull("identifier").NullToEmpty();
            var category = aNode?.GetAttributeOrNull("category").NullToEmpty();
            var tags = aNode?.GetAttributeOrNull("Tags").NullToEmpty();
            var group = "Items";
            if (aNode?.OwnerDocument?.DocumentElement?.Name == "Character")
            {
                group = "Creatures";
            }
            else if (id == "fpgacircuit") // This one has lots of component connections but isn't one.
            {
                group = "Components";
            }
            else if (id.EndsWith("component"))
            {
                group = "Components";
            }
            else if (tags.Contains("plant"))
            {
                group = "Plants";
            }
            else if (tags.Contains("medical"))
            {
                group = "Medical";
            }
            else if (category == "Material")
            {
                group = "Materials";
                if (tags.Contains("ore"))
                {
                    group = "Ore";
                }
                else if (_metals.Contains(id))
                {
                    group = "Metal";
                }
            }
            else if (category == "Equipment")
            {
                group = "Equipment";
            }

            return group;
        }


        // Possible future improvement: Move the metals that come in small bottles to a new group.
        private static HashSet<string> _metals = new HashSet<string>
        {
            "iron",
            "tin",
            "lead",
            "copper",
            "zinc",
            "aluminum",
            "uranium",
            "titanium",
            "magnesium",
            "thorium",
            "steel",
            "titaniumaluminiumalloy",
            "sodium",
            "calcium",
            "potassium",
            "lithium",
            "physicorium",
            "scrap1",
            "scrap2",
            "scrap3",
            "scrap4",
            "scrap5",
            "scrap6",
            "scrap7",
        };
    }
}
