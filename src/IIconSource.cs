﻿using System.Drawing;
using System.Xml;

namespace BaroTree
{
    /// <summary>
    /// Interface for objects that can provide paths to node icons.
    /// </summary>
    public interface IIconSource
    {
        /// <summary>
        /// Maximum number of pixels in either width or height of generated
        /// icons. Icons smaller than this will be unmodified; those larger
        /// will be proportionally resized so that the longest side length is 
        /// the same as this value.
        /// </summary>
        int MaxIconSize { get; set; }


        /// <summary>
        /// Get the path to an extract icon image as specified in typical game XmlNodes.
        /// </summary>
        /// <param name="aNode">Game entity node from the game data files.</param>
        /// <returns>Path to the extracted icon image. May be the same as previously generated icons.</returns>
        string GetIconPath(XmlNode aNode);


        /// <summary>
        /// Get the path to an extracted icon image.
        /// </summary>
        /// <param name="aSourceImagePath">Path to the atlas texture to read from.</param>
        /// <param name="aPatch">Region of the atlas to extract to an icon.</param>
        /// <param name="aTint">Optional tint color; defaults to white.</param>
        /// <returns>Path to the extracted icon image. May be the same as previously generated icons.</returns>
        string GetIconPath(string aSourceImagePath, Rectangle aPatch, Color? aTint = null);
    }
}
