﻿using CowTools.Xml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace BaroTree
{
    /// <summary>
    /// Helper class for extracting individual icons from game atlas images and
    /// resizing them if too large. Caches info about generated image files to 
    /// avoid generating duplicates, but this only applies within the lifetime
    /// of one instance; duplicates will still be generated across multiple runs.
    /// </summary>
    public class IconExtractor : IIconSource
    {
        /// <summary>
        /// Initialize the instance.
        /// </summary>
        /// <param name="aInputPath">Base path for resolving input images. All
        /// source image paths are expected to be relative to this.</param>
        /// <param name="aOutputPath">Directory to store extracted icon images in.</param>
        public IconExtractor(string aInputPath, string aOutputPath)
        {
            _inPath = aInputPath;
            _outPath = aOutputPath;
        }


        #region -- IIconSource implementation --

        /// <inheritdoc/>
        public int MaxIconSize { get; set; } = 64;


        /// <inheritdoc/>
        public string GetIconPath(XmlNode aNode)
        {
            string result = null;
            var spriteNode = aNode?.FindChildXmlNodes("Sprite")?.FirstOrDefault();
            if (spriteNode is null)
            {
                spriteNode = aNode?.FindChildXmlNodes("InventoryIcon")?.FirstOrDefault();
            }

            if (spriteNode != null)
            {
                var fileName = spriteNode.GetAttributeOrNull("texture");
                var resolvedPath = fileName;
                if (!string.IsNullOrEmpty(resolvedPath))
                {
                    resolvedPath = Path.Combine(_inPath, resolvedPath);

                    // Sprite nodes have paths relative to the XML file rather than the game content dir.
                    if (!File.Exists(resolvedPath))
                    {
                        var uri = new Uri(aNode.OwnerDocument.BaseURI);
                        var dirName = Path.GetDirectoryName(uri.LocalPath);
                        resolvedPath = Path.Combine(dirName, fileName);
                    }
                }

                if (File.Exists(resolvedPath))
                {
                    // Some of the icons have to have a color mask applied (wires).
                    var tint = Color.White;
                    var colorStr = aNode.GetAttributeOrNull("spritecolor") ?? string.Empty;
                    var colorMatch = _colorMatch.Match(colorStr);
                    if (colorMatch.Success && (colorMatch.Groups.Count == 5))
                    {
                        tint = Color.FromArgb(
                            int.Parse(colorMatch.Groups[1].Value),
                            int.Parse(colorMatch.Groups[2].Value),
                            int.Parse(colorMatch.Groups[3].Value));
                    }

                    var rectStr = spriteNode.GetAttributeOrNull("sourcerect") ?? string.Empty;
                    var rectMatch = _rectMatch.Match(rectStr);
                    if (rectMatch.Success && (rectMatch.Groups.Count == 5))
                    {
                        var x = int.Parse(rectMatch.Groups[1].Value);
                        var y = int.Parse(rectMatch.Groups[2].Value);
                        var w = int.Parse(rectMatch.Groups[3].Value);
                        var h = int.Parse(rectMatch.Groups[4].Value);
                        var rect = new Rectangle(x, y, w, h);
                        return GetIconPath(resolvedPath, rect, tint);
                    }
                }
            }

            return result;
        }


        /// <inheritdoc/>
        public string GetIconPath(string aSourceImagePath, Rectangle aPatch, Color? aTint = null)
        {
            if (string.IsNullOrEmpty(aSourceImagePath) || !File.Exists(aSourceImagePath))
            {
                return null;
            }

            if (!_loadedImages.TryGetValue(aSourceImagePath, out var atlas))
            {
                atlas = Bitmap.FromFile(aSourceImagePath) as Bitmap;
                _loadedImages[aSourceImagePath] = atlas;
            }

            return GetIcon(atlas, aSourceImagePath, aPatch, aTint ?? Color.White);
        }

        #endregion


        private string GetIcon(Bitmap aImage, string aSourcePath, Rectangle aRect, Color aTint)
        {
            var cacheKey = new Tuple<string, Rectangle, Color>(aSourcePath, aRect, aTint);
            var namePrefix = Path.GetFileNameWithoutExtension(aSourcePath);
            if (!_outputCache.TryGetValue(cacheKey, out var fileName))
            {
                int suffix = 1;
                fileName = $"{namePrefix}_{suffix}.png";
                while (File.Exists(Path.Combine(_outPath, fileName)))
                {
                    suffix++;
                    fileName = $"{namePrefix}_{suffix}.png";
                }

                var oneInTwoFiddy = 1.0f / 255.0f;
                var tintMat = new ColorMatrix
                {
                    Matrix00 = oneInTwoFiddy * aTint.R,
                    Matrix11 = oneInTwoFiddy * aTint.G,
                    Matrix22 = oneInTwoFiddy * aTint.B,
                    Matrix33 = 1.0f,
                    Matrix44 = 1.0f
                };
                var drawAttr = new ImageAttributes();
                drawAttr.SetColorMatrix(tintMat);

                // Resize if too large.
                var scale = (double)MaxIconSize / (double)Math.Max(aRect.Width, aRect.Height);
                scale = Math.Min(1.0, scale);
                var newWidth = (int)(scale * aRect.Width);
                var newHeight = (int)(scale * aRect.Height);
                using (var scaled = new Bitmap(newWidth, newHeight, PixelFormat.Format32bppArgb))
                {
                    using (var g = Graphics.FromImage(scaled))
                    {
                        g.DrawImage(aImage, new Rectangle(0, 0, newWidth, newHeight), aRect.X, aRect.Y, aRect.Width, aRect.Height, GraphicsUnit.Pixel, drawAttr);
                    }

                    scaled.Save(Path.Combine(_outPath, fileName), ImageFormat.Png);
                }

                _outputCache[cacheKey] = fileName;
            }

            return fileName;
        }


        // Rectangles and colors in the XML files are expected to be in x,y,w,h format.
        private static Regex _rectMatch = new Regex(@"(\d+),(\d+),(\d+),(\d+)", RegexOptions.Compiled);
        private static Regex _colorMatch = new Regex(@"(\d+),(\d+),(\d+),(\d+)", RegexOptions.Compiled);

        private string _inPath;
        private string _outPath;
        private Dictionary<string, Bitmap> _loadedImages = new Dictionary<string, Bitmap>();
        private Dictionary<Tuple<string, Rectangle, Color>, string> _outputCache = new Dictionary<Tuple<string, Rectangle, Color>, string>();
    }
}
