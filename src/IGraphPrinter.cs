﻿namespace BaroTree
{
    /// <summary>
    /// Interface for graph output converters.
    /// </summary>
    public interface IGraphPrinter
    {
        /// <summary>
        /// Path to the directory to write output files to.
        /// </summary>
        string OutputDirectory { get; set; }


        /// <summary>
        /// Write out a graph.
        /// </summary>
        void WriteOutput(IGraph aGraph);
    }
}
