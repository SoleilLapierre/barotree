﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace BaroTree
{
    [DisplayName("unified")]
    public class PumlPrinterByUnified : IGraphPrinter
    {
        #region -- IGraphPrinter implementation --

        public string OutputDirectory { get; set; }

        public void WriteOutput(IGraph aGraph)
        {
            if (string.IsNullOrEmpty(OutputDirectory))
            {
                throw new ArgumentException("Output directory is required.");
            }

            if (!Directory.Exists(OutputDirectory))
            {
                Directory.CreateDirectory(OutputDirectory);
            }

            _graph = aGraph;
            _usedNodes = new HashSet<string>();
            foreach (var edge in _graph.Edges)
            {
                _usedNodes.Add(edge.Producer);
                _usedNodes.Add(edge.Consumer);
            }

            using (var file = File.Open(Path.Combine(OutputDirectory, "Barotrauma.puml"), FileMode.Create))
            {
                file.Write(Encoding.ASCII.GetBytes(GenerateText()));
            }
        }

        #endregion

        private string GenerateText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("@startuml");
            sb.AppendLine();
            sb.AppendLine("left to right direction");
            sb.AppendLine();

            foreach (var (key, node) in _graph.Nodes)
            {
                if (_usedNodes.Contains(key))
                {
                    sb.AppendLine($"object \"{node.Name}\" as {key}");

                    if (!string.IsNullOrEmpty(node.ImagePath))
                    {
                        sb.AppendLine($"{key} : <img:{node.ImagePath}>");
                    }
                }
            }

            sb.AppendLine();

            void EmitEdge(Edge aEdge, string aProducer, string aConsumer)
            {
                var count = aEdge.MaxCount.ToString();
                if (aEdge.MaxCount != aEdge.MinCount)
                {
                    count = $"{aEdge.MinCount}-{aEdge.MaxCount}";
                }
                else if ((aEdge.Probability < 1.0f) && (aEdge.Probability > 0.0f))
                {
                    count = ((int)Math.Round(100.0f * aEdge.Probability)).ToString() + "%";
                }

                sb.AppendLine($"{aProducer} --> {aConsumer} : \"{aEdge.Action} ({count})\"");
            }


            foreach (var edge in _graph.Edges)
            {
                EmitEdge(edge, edge.Producer, edge.Consumer);
            }

            sb.AppendLine();
            sb.AppendLine("@enduml");
            return sb.ToString();
        }

        private IGraph _graph;
        private HashSet<string> _usedNodes = new HashSet<string>();
    }
}
