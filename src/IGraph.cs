﻿using System.Collections.Generic;

namespace BaroTree
{
    /// <summary>
    /// Interface for the crafting graph overall.
    /// </summary>
    public interface IGraph
    {
        /// <summary>
        /// All edges in the graph.
        /// </summary>
        IEnumerable<Edge> Edges { get; }


        /// <summary>
        /// All nodes in the graph.
        /// </summary>
        IReadOnlyDictionary<string, Node> Nodes { get; }
    }
}


