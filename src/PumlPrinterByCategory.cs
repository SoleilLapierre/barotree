﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace BaroTree
{
    [DisplayName("by-category")]
    public class PumlPrinterByCategory : IGraphPrinter
    {
        #region -- IGraphPrinter implementation --

        public string OutputDirectory { get; set; }

        public void WriteOutput(IGraph aGraph)
        {
            if (string.IsNullOrEmpty(OutputDirectory))
            {
                throw new ArgumentException("Output directory is required.");
            }

            if (!Directory.Exists(OutputDirectory))
            {
                Directory.CreateDirectory(OutputDirectory);
            }

            _graph = aGraph;
            _usedNodes = new HashSet<string>();
            foreach (var edge in _graph.Edges)
            {
                _usedNodes.Add(edge.Producer);
                _usedNodes.Add(edge.Consumer);
            }

            foreach (var groupName in _graph.Nodes.Where(p => _usedNodes.Contains(p.Key)).Select(p => p.Value.Group).Distinct())
            {
                using (var file = File.Open(Path.Combine(OutputDirectory, $"{groupName}.puml"), FileMode.Create))
                {
                    file.Write(Encoding.ASCII.GetBytes(GenerateText(groupName)));
                }
            }
        }

        #endregion

        private string GenerateText(string aGroup)
        {
            var sb = new StringBuilder();
            sb.AppendLine("@startuml");
            sb.AppendLine();
            sb.AppendLine("left to right direction");
            sb.AppendLine();

            int copyCount = 0;
            var mainNodes = new HashSet<string>();
            foreach (var (key, node) in _graph.Nodes.Where(p => (p.Value.Group == aGroup) && _usedNodes.Contains(p.Value.ID)))
            {
                sb.AppendLine($"object \"{node.Name}\" as {key}");
                mainNodes.Add(key);

                if (!string.IsNullOrEmpty(node.ImagePath))
                {
                    sb.AppendLine($"{key} : <img:{node.ImagePath}>");
                }
            }

            sb.AppendLine();

            string EmitDummyNode(string aName, string aImagePath)
            {
                var tmpNodeName = $"_dummy{copyCount++}";
                sb.AppendLine($"object \"{aName}\" as {tmpNodeName}");

                if (!string.IsNullOrEmpty(aImagePath))
                {
                    sb.AppendLine($"{tmpNodeName} : <img:{aImagePath}>");
                }

                return tmpNodeName;
            }

            void EmitEdge(Edge aEdge, string aProducer, string aConsumer)
            {
                var count = aEdge.MaxCount.ToString();
                if (aEdge.MaxCount != aEdge.MinCount)
                {
                    count = $"{aEdge.MinCount}-{aEdge.MaxCount}";
                }
                else if ((aEdge.Probability < 1.0f) && (aEdge.Probability > 0.0f))
                {
                    count = ((int)Math.Round(100.0f * aEdge.Probability)).ToString() + "%";
                }

                sb.AppendLine($"{aProducer} --> {aConsumer} : \"{aEdge.Action} ({count})\"");
            }


            foreach (var edge in _graph.Edges)
            {
                var producer = edge.Producer;
                var consumer = edge.Consumer;

                if (mainNodes.Contains(producer))
                {
                    var consumerName = consumer;
                    if (_graph.Nodes.TryGetValue(consumer, out var consumerNode))
                    {
                        consumerName = consumerNode.Name;
                    }

                    var tmpConsumer = EmitDummyNode(consumerName, consumerNode?.ImagePath);
                    EmitEdge(edge, producer, tmpConsumer);
                }

                if (mainNodes.Contains(consumer))
                {
                    var producerName = producer;
                    if (_graph.Nodes.TryGetValue(producer, out var producerNode))
                    {
                        producerName = producerNode.Name;
                    }

                    var tmpProducer = EmitDummyNode(producerName, producerNode?.ImagePath);
                    EmitEdge(edge, tmpProducer, consumer);
                }
            }

            sb.AppendLine();
            sb.AppendLine("@enduml");
            return sb.ToString();
        }

        private IGraph _graph;
        private HashSet<string> _usedNodes = new HashSet<string>();
    }
}
