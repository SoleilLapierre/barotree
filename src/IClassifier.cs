﻿using System.Xml;

namespace BaroTree
{
    /// <summary>
    /// Interface for objects that classify game data nodes.
    /// </summary>
    public interface IClassifier
    {
        string Classify(XmlNode aNode);
    }
}
