﻿namespace BaroTree
{
    /// <summary>
    /// Information about a graph edge (production or consumption of resources).
    /// </summary>
    public class Edge
    {
        /// <summary>
        /// ID (resource name) of the source node.
        /// </summary>
        public string Producer { get; set; }


        /// <summary>
        /// ID of the destination node.
        /// </summary>
        public string Consumer { get; set; }


        /// <summary>
        /// Name for the edge.
        /// </summary>
        public string Action { get; set; }


        /// <summary>
        /// Minumum number of resources produced or consumed on this edge.
        /// </summary>
        public int MinCount { get; set; } = 1;


        /// <summary>
        /// Maximum number of resources produced or consumed on this edge.
        /// </summary>
        public int MaxCount { get; set; } = 1;


        /// <summary>
        /// Probability of this edge existing.
        /// </summary>
        public float Probability { get; set; } = 1.0f;
    }
}
