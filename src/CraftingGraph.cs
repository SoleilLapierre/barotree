﻿using CowTools.Extensions;
using CowTools.Xml;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace BaroTree
{
    /// <summary>
    /// Generates graph data from the game files.
    /// </summary>
    public class CraftingGraph : IGraph, IGraphGenerator
    {
        /// <summary>
        /// Initialize the generator.
        /// </summary>
        /// <param name="aPath">Path to the root directory for the game files.</param>
        /// <param name="aIconSource">Helper to get icon images for nodes.</param>
        public CraftingGraph(string aPath, IIconSource aIconSource)
        {
            _inputPath = aPath;
            _images = aIconSource;
        }


        #region -- IGraphGenerator implementation --

        /// <inheritdoc/>
        public IClassifier Classifier { get; set; } = new DefaultClassifier();


        /// <inheritdoc/>
        public void Update()
        {
            _nodes = new Dictionary<string, Node>();
            _edges = new List<Edge>();
            LoadNames("English"); // Future expansion: Support other languages.
            ScanContent(_inputPath);
        }


        /// <inheritdoc/>
        public IGraph Graph { get => this; }

        #endregion
        #region -- IGraph implementation --

        /// <inheritdoc/>
        public IEnumerable<Edge> Edges => _edges;


        /// <inheritdoc/>
        public IReadOnlyDictionary<string, Node> Nodes => _nodes;

        #endregion

        // Precache human-readable names for some entity IDs.
        private void LoadNames(string aLanguage)
        {
            _nameMap = new Dictionary<string, string>();
            var filePath = Path.Join(_inputPath, "Texts", aLanguage, aLanguage + "Vanilla.xml");

            try
            {
                var doc = new XmlDocument();
                doc.Load(filePath);
                foreach (var element in doc.DocumentElement.GetAllChildXmlNodes())
                {
                    var name = element.Name;
                    if (!string.IsNullOrEmpty(name) && (element.Name.StartsWith("character.") || element.Name.StartsWith("entityname.")))
                    {
                        name = name.Substring(name.IndexOf('.') + 1);
                        var value = element.InnerText;
                        if (!string.IsNullOrEmpty(value))
                        {
                            _nameMap[name] = value;
                        }
                    }
                }
            }
            catch (Exception)
            {
                _log.Warn($"Could not parse {filePath}");
                throw;
            }
        }


        // Recursively scan all game XML files.
        private void ScanContent(string aPath)
        {
            foreach (var file in Directory.GetFiles(aPath, "*.xml"))
            {
                _log.Info($"Scanning {file}...)");
                Parse(file);
            }

            foreach (var dirName in Directory.GetDirectories(aPath))
            {
                ScanContent(dirName);
            }
        }


        private void Parse(string aFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(aFileName);
                var docType = doc.DocumentElement.Name;
                switch (docType)
                {
                    case "Items":
                        ParseItems(doc);
                        break;
                    case "Character":
                        ParseCharacter(doc);
                        break;
                    default:
                        _log.Debug($"No handler for doc root type {docType ?? "<null>"}");
                        break;
                }
            }
            catch (Exception e)
            {
                _log.Warn($"Could not parse {aFileName}");
                _log.Debug("Exception", e);
            }
        }


        // Scan a document with an Items root node.
        private void ParseItems(XmlDocument aDoc)
        {
            var itemList = aDoc.SelectSingleNode("Items");
            foreach (var node in itemList.GetAllChildXmlNodes())
            {
                var id = node.GetAttributeOrNull("identifier");
                if (string.IsNullOrEmpty(id))
                {
                    continue;
                }

                // Try to come up with a new name for the item node.
                var typeName = node.Name;
                var name = node.GetAttributeOrNull("name").EmptyToNull();
                if (typeName == "Item")
                {
                    typeName = null;
                }

                if (!_nameMap.TryGetValue(id, out var itemName))
                {
                    itemName = null;
                }

                if (string.IsNullOrEmpty(itemName))
                {
                    itemName = typeName ?? name ?? id;
                }


                var group = Classifier.Classify(node);

                var newItem = new Node
                {
                    ID = id,
                    Name = itemName,
                    Group = group,
                    ImagePath = _images?.GetIconPath(node),
                };

                _nodes[id] = newItem;
                _log.DebugFormat("- Item {0} ({1})", typeName, id);


                void CountID(XmlNode aNode, IDictionary<string, int> aCounts)
                {
                    var itemId = aNode.GetAttributeOrNull("identifier");
                    if (!string.IsNullOrEmpty(itemId))
                    {
                        if (!aCounts.TryGetValue(itemId, out var count))
                        {
                            aCounts[itemId] = 0;
                        }

                        aCounts[itemId] = aCounts[itemId] + 1;
                    }
                }

                // Create deconstruction edges.
                var drops = new Dictionary<string, int>();
                foreach (var child in node.FindChildXmlNodes("Deconstruct"))
                {
                    foreach (var item in child.GetAllChildXmlNodes())
                    {
                        CountID(item, drops);
                    }
                }

                foreach (var (itemId, count) in drops)
                {
                    _edges.Add(new Edge
                    {
                        Action = $"Deconstruct",
                        Producer = id,
                        Consumer = itemId,
                        MaxCount = count,
                        MinCount = count,
                    });

                    _log.DebugFormat("  - deconstruct -> {0}", itemId);
                }

                // Create fabrication edges.
                var ingredients = new Dictionary<string, int>();
                foreach (var child in node.FindChildXmlNodes("Fabricate"))
                {
                    foreach (var item in child.FindChildXmlNodes("RequiredItem"))
                    {
                        CountID(item, ingredients);
                    }
                }

                foreach (var (itemId, count) in ingredients)
                {
                    _edges.Add(new Edge
                    {
                        Action = $"Fabricate",
                        Producer = itemId,
                        Consumer = id,
                        MaxCount = count,
                        MinCount = count,
                    });

                    _log.DebugFormat("  - consumes <- {0}", itemId);
                }
            }
        }


        // Scan a document with a Character root node.
        private void ParseCharacter(XmlDocument aDoc)
        {
            var rootNode = aDoc.FirstChild;
            string species = rootNode.GetAttributeOrNull("speciesname");

            if (!_nameMap.TryGetValue(species.ToLowerInvariant(), out var speciesName))
            {
                speciesName = species;
            }

            if (!string.IsNullOrEmpty(species))
            {
                _log.DebugFormat("- Character: {0}", speciesName);
                var speciesNode = new Node
                {
                    ID = species,
                    Name = speciesName,
                    Group = Classifier.Classify(rootNode),
                    ImagePath = _images?.GetIconPath(rootNode),
                };

                _nodes[speciesNode.ID] = speciesNode;

                // First int is max quantity, second is probability chances.
                var drops = new Dictionary<string, Tuple<int, int>>();
                int totalProb = 0;
                foreach (var inventory in rootNode.FindChildXmlNodes("Inventory"))
                {
                    int commonness;
                    var commonnessStr = inventory.GetAttributeOrNull("commonness").NullToEmpty();
                    if (!int.TryParse(commonnessStr, out commonness))
                    {
                        commonness = 0;
                    }

                    totalProb += commonness;

                    var countThisInv = new Dictionary<string, int>();
                    foreach (var item in inventory.FindChildXmlNodes("Item"))
                    {
                        var id = item.GetAttributeOrNull("identifier");
                        if (!string.IsNullOrEmpty(id))
                        {
                            if (!drops.TryGetValue(id, out var drop))
                            {
                                drop = new Tuple<int, int>(0, 0);
                                drops[id] = drop;
                            }

                            if (!countThisInv.ContainsKey(id))
                            {
                                countThisInv[id] = 0;
                            }

                            countThisInv[id] = countThisInv[id] + 1;
                            drop = new Tuple<int, int>(Math.Max(drop.Item1, countThisInv[id]), drop.Item2 + commonness);
                            drops[id] = drop;
                        }
                    }
                }

                foreach (var (id, count) in drops)
                {
                    _edges.Add(new Edge
                    {
                        Action = $"Drop",
                        Producer = species,
                        Consumer = id,
                        MaxCount = drops[id].Item1,
                        Probability = (float)drops[id].Item2 / (float)totalProb,
                    });

                    _log.Debug($"  - Drops {id}");
                }
            }
        }


        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        private string _inputPath;
        private Dictionary<string, Node> _nodes = new Dictionary<string, Node>();
        private List<Edge> _edges = new List<Edge>();
        private Dictionary<string, string> _nameMap = new Dictionary<string, string>();
        private IIconSource _images;
    }
}
