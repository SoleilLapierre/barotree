﻿using CowTools.Reflection;
using log4net;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BaroTree
{
    /// <summary>
    /// This program scans Barotrauma game content files to build a crafting tree, and outputs
    /// the tree to PlantUML format. You must use PlantUML separately to convert the output
    /// file to an image.
    /// </summary>
    public class Program
    {
        static int Main(string[] aArgs)
        {
            string outputStyle = "by-category";
            string gamePath = null;
            string outPath = Directory.GetCurrentDirectory();
            var iconSize = 64;

            // Parse command line arguments.
            var i = 0;
            while (i < aArgs.Length - 1)
            {
                if (aArgs[i] == "-i")
                {
                    gamePath = aArgs[i + 1];
                    i++;
                }
                else if (aArgs[i] == "-o")
                {
                    outPath = aArgs[i + 1];
                    i++;
                }
                else if (aArgs[i] == "-iconsize")
                {
                    iconSize = int.Parse(aArgs[i + 1]);
                    iconSize = Math.Min(iconSize, 1024);
                    i++;
                }
                else if (aArgs[i] == "-style")
                {
                    outputStyle = aArgs[i + 1];
                    i++;
                }
                else if ((aArgs[i] == "-?") || (aArgs[i] == "-h") || (aArgs[i] == "-help"))
                {
                    Console.WriteLine("Usage:");
                    Console.WriteLine("BaroTree [-p path] [-o path] [-?] [-iconsize n]");
                    Console.WriteLine("Where -p specifies the path to the directory containing barotrauma.exe,");
                    Console.WriteLine("         if it not found automatically.");
                    Console.WriteLine("      -o specifies the output directory. Default is the current");
                    Console.WriteLine("         working directory.");
                    Console.WriteLine("      -? displays this help.");
                    Console.WriteLine("      -iconsize specifies the maximum size of node icons.");
                    Console.WriteLine("         Specify 0 to disable icons. Smaller icons will not be enlarged.");
                    Console.WriteLine("      -style specifies the output style:");
                    Console.WriteLine("         by-category (default) Split output graph by category.");
                    Console.WriteLine("         unified Add all output to a single image.");
                    return -1;
                }

                i++;
            }


            // Autodetect game file path if not specified on command line.
            if (string.IsNullOrEmpty(gamePath))
            {
                try
                {
                    gamePath = DetectGameLocation();
                }
                catch (Exception)
                {
                    _log.Error("FAIL: Failed to auto-detect game install location.");
                    throw;
                }
            }

            var contentPath = Path.Combine(gamePath ?? string.Empty, "Content");
            if (!Directory.Exists(contentPath))
            {
                _log.Error("FAIL: Game content directory not found.");
                return -1;
            }

            if (!Directory.Exists(outPath))
            {
                try
                {
                    Directory.CreateDirectory(outPath);
                }
                catch (Exception)
                {
                    _log.Error($"FAIL: Could not create output directory '{outPath}'.");
                    throw;
                }
            }

            _log.Info($"Game install location: {gamePath}");
            _log.Info($"Output directory: {outPath}");

            // Initialize icon extractor.
            IIconSource iconSource = null;
            if (iconSize > 0)
            {
                iconSource = new IconExtractor(gamePath, outPath);
                iconSource.MaxIconSize = iconSize;
            }

            // Generate the graph.
            IGraphGenerator tree = new CraftingGraph(contentPath, iconSource);
            tree.Update();

            // Output the graph.
            IGraphPrinter printer = CreatePrinter(outputStyle);
            if (printer is null)
            {
                _log.Error($"Unrecognized output format '{outputStyle}'.");
                return -1;
            }

            printer.OutputDirectory = outPath;
            printer.WriteOutput(tree.Graph);

            _log.Info("Finished!");
            return 0;
        }


        // Try to find the Steam game install location using registry lookups.
        private static string DetectGameLocation()
        {
            var root = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default);
            var key = root.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 602960");
            var name = key.GetValue("DisplayName") as string;
            if (name != "Barotrauma")
            {
                _log.Warn("Game registry keys not as expected.");
                return null;
            }

            var path = key.GetValue("InstallLocation") as string;
            if (Directory.Exists(path))
            {
                return path;
            }

            return null;
        }


        private static IGraphPrinter CreatePrinter(string aStyle)
        {
            var reg = new TypeRegistry();
            reg.Register(typeof(Program).Assembly);
            var generators = reg.FindImplementationsOf<IGraphPrinter>();
            var genType = generators.Where(t =>
            {
                if (t.IsAbstract)
                {
                    return false;
                }

                var attr = t.GetCustomAttributes<DisplayNameAttribute>(true).FirstOrDefault(a => a.DisplayName == aStyle);
                return (attr != null);
            }).FirstOrDefault();

            IGraphPrinter gen = null;
            if (genType != null)
            {
                gen = Activator.CreateInstance(genType) as IGraphPrinter;
            }

            return gen;
        }


        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);
    }
}
