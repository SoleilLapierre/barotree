﻿namespace BaroTree
{
    /// <summary>
    /// Base class for entities parsed from XML files.
    /// </summary>
    public class Node
    {
        /// <summary>
        /// Top-level type identifier - item, character etc.
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Game's entity ID.
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// Grouping to help organize graph.
        /// </summary>
        public string Group { get; set; }


        /// <summary>
        /// Path to the node icon image file, if any.
        /// </summary>
        public string ImagePath { get; set; }
    }
}
