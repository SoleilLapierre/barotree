﻿namespace BaroTree
{
    /// <summary>
    /// Interface for the crafting graph generator.
    /// </summary>
    public interface IGraphGenerator
    {
        /// <summary>
        /// Helper
        /// </summary>
        IClassifier Classifier { get; set; }


        /// <summary>
        /// Start graph generation.
        /// </summary>
        void Update();


        /// <summary>
        /// Get generated graph.
        /// </summary>
        IGraph Graph { get; }
    }
}


