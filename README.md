# BaroTree

After noticing that the [Barotrauma](https://barotraumagame.com/) game files are in XML format, I wanted
to see if I could use the data to generate a useful crafting tree as an image, to make it easy to know what
resource items are needed to craft a desired item. It idea was to generate the nodes and edges of a graph
from the XML files and feed it into [PlantUML](https://plantuml.com/), my favorite graph generation tool,
to get the output.

The full crafting tree proved to be too complex to usefully represent in a single image, so instead
I split it up into several images by category, showing only the local crafting recipes for the items
as well as their deconstruction results.

## Not maintained

Please note this was just an experimental hobby project and not a utility I plan to maintain and update.
The source code is [MIT licensed](LICENSE.md); you're welcome to fork and extend this project. 

The functionality is likely to break as the game itself evolves; the organization and content of the game
data files this program uses as input are subject to change.

## Known issues

* Output includes empty nodes fo rsome non-craftable items.
* Main nodes in the output images are not in any particular order. This is decided by GraphViz.
* Deconstruction results for a few items are duplicated. This is because they are defined
  multiple times in the game files.

# Results

You don't need to compile and run the program; the results from build ID 5753888 of the game are below (build IDs 
are visible on the Local Files tab of the game properties in your Steam library).

Note these are very large images. I set up PlantUML to produce tall rather than wide images so that
only vertical scrolling is needed.

The game item icons included in these images are not mine; they are generated from the game art and are 
the intellectual property of the Barotrauma developers.

I recommend downloading these images and using your favorite offline image viewer on them.

[Components](Components.png)
[Creatures](Creatures.png)
[Equipment](Equipment.png)
[Items](Items.png)
[Materials](Materials.png)
[Medical](Medical.png)
[Metal](Metal.png)
[Ore](Ore.png)
[Plants](Plants.png)

[Combined](Combined.jpg) is all of the above images manually composited into one.

There is also an option generate a single graph, which results in output that isn't all that useful
because of overlapping lines:

[Barotrauma](Barotrauma.png)

This was the output format I had originally envisioned, until I realized a single large image wasn't going to
be useful as a reference.


# Running the program

See the [example batch file](generate.bat). The general idea is to compile the program, run it, then run PlantUML on
its output. 

The program will attempt to locate the Barotrauma game files automatically but if that fails there
is a command line option to specify the location. The program produces several .puml files and many .png
images as intermediate output.

After running, use `java -DPLANTUML_LIMIT_SIZE=16384 -jar plantuml.jar *.puml` in the output directory to
generate the final images. They will be named after the puml files.

Note that running PlantUML in "server mode" where it watches a directory and automatically regenerates
images when the puml files change will not work correctly, as it cannot read the icon images from the
local file system in that mode.

You are responsible for getting PlantUML working on your system; it can be tricky as it depends on GraphViz,
and certain versions of that are not fully compatible. You will have to edit the example batch file to point
to the install location of PlantUML on your system.

