dotnet build src\BaroTree.sln -c Debug
src\bin\Debug\netcoreapp3.1\BaroTree.exe -o src\bin\Debug\netcoreapp3.1\output
java -DPLANTUML_LIMIT_SIZE=32768 -jar c:\utils\plantuml.jar -o graphs src\bin\Debug\netcoreapp3.1\output
move src\bin\Debug\netcoreapp3.1\output\graphs\*.png .
del /Q src\bin\Debug\netcoreapp3.1\output\*.*
